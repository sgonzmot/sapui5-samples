sap.ui.define(
  [
    "com/santiago/Samples/controller/BaseController"
  ],
  function (BaseController) {
    "use strict";

    return BaseController.extend("com.santiago.Samples.controller.MainView", {
      goInput: function () {
        this.navTo("Inputs", null, null);
      },
    });
  }
);

sap.ui.define(
  [
    "com/santiago/Samples/controller/BaseController",
    "sap/ui/model/json/JSONModel",
    "com/santiago/Samples/model/models",
    "sap/ui/core/Fragment",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator",
  ],
  function (
    BaseController,
    JSONModel,
    models,
    Fragment,
    Filter,
    FilterOperator
  ) {
    "use strict";

    return BaseController.extend("com.santiago.Samples.controller.Inputs", {
      onInit: function () {
        var oModel = new JSONModel(models.getProducts());
        // the default limit of the model is set to 100. We want to show all the entries.
        oModel.setSizeLimit(100000);
        this.getView().setModel(oModel);
      },

      onValueHelpRequest: function (oEvent) {
        var sInputValue = oEvent.getSource().getValue(),
          oView = this.getView();

        if (!this._pValueHelpDialog) {
          this._pValueHelpDialog = Fragment.load({
            id: oView.getId(),
            name: "com.santiago.Samples.view.fragments.Dialog",
            controller: this,
          }).then(function (oDialog) {
            oView.addDependent(oDialog);
            return oDialog;
          });
        }
        this._pValueHelpDialog.then(function (oDialog) {
          oDialog
            .getBinding("items")
            .filter([new Filter("Name", FilterOperator.Contains, sInputValue)]);
          oDialog.open(sInputValue);
        });
      },
      onValueHelpDialogSearch: function (oEvent) {
        var sValue = oEvent.getParameter("value");
        var oFilter = new Filter("Name", FilterOperator.Contains, sValue);

        oEvent.getSource().getBinding("items").filter([oFilter]);
      },

      onValueHelpDialogClose: function (oEvent) {
        var sDescription,
          oSelectedItem = oEvent.getParameter("selectedItem");
        oEvent.getSource().getBinding("items").filter([]);

        if (!oSelectedItem) {
          return;
        }

        sDescription = oSelectedItem.getDescription();

        this.byId("productInput").setSelectedKey(sDescription);
        this.byId("selectedKeyIndicator").setText(sDescription);
      },

      onSuggestionItemSelected: function (oEvent) {
        var oItem = oEvent.getParameter("selectedItem");
        var oText = oItem ? oItem.getKey() : "";
        this.byId("selectedKeyIndicator").setText(oText);
      },
    });
  }
);

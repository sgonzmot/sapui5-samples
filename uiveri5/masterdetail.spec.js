require("./pages/master");

describe("masterdetail", function () {

  it("should see the app", function () {
    expect(browser.getTitle()).toBe('Samples');
  });

  it("should see the Master page", async function () {
    let oButton = await element(by.control({
      viewName: 'com.santiago.Samples.view.MainView',
      id: 'idButton'
    }))
    browser.actions().sendKeys(protractor.Key.ENTER).perform();

    let text = await oButton.getText();

    expect(text).toBe("test")
  });
});
